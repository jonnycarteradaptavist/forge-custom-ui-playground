import Resolver from '@forge/resolver'

const resolver = new Resolver()

console.log("Let's define a custom macro config")
resolver.define('getConfig', (req) => {
    console.log(req)

    return "here's a thing"
    /*(
        <>
            <p>Here is the stuff.</p>
            <form>
                <label htmlFor="someData">Some Data:</label>
                <input type="text" name="someData" id="someData" onChange={(event) => {
                    console.log("Here's the event", event)
                }}/>
            </form>
        </>
    )
     */
})
console.log("Here's the resolver", resolver)

export const handler = resolver.getDefinitions()
console.log("Here's the handler", handler)
