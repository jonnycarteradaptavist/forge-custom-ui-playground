import Resolver from '@forge/resolver';

const resolver = new Resolver();

resolver.define('getText', (req) => {
  console.log(req);

  return 'Hello, world!';
});

console.log("Here's the resolver", resolver)

export const handler = resolver.getDefinitions()
console.log("Here's the handler", handler)
